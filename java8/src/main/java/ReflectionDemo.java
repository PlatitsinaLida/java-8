import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {

    //Дан список объектов произвольных типов. Найдите количество элементов списка,
    //которые являются объектами типа Human или его подтипами.
    public static int countHuman(List<Object> list){
        int result = 0;
        for (Object elem: list){
            if(elem instanceof Human){
                result++;
            }
        }
        return result;
    }

    //Для объекта получить список имен его открытых методов.
    public static List<String> listOfPublicMethod(Object obj){
        Class one=obj.getClass();
        List<String> result = new ArrayList<>();
        for (Method method:one.getMethods()){
            result.add(method.getName());
        }
        return result;
    }

    // Для объекта получить список (в виде списка строк) имен
    // всех его суперклассов до класса Object включительно.
    public static List<String> listOfSuperClass(Object obj){
        Class one = obj.getClass().getSuperclass();
        List<String> result = new ArrayList<>();
        while (!(one==null)){
            result.add(one.getSimpleName());
            one = one.getSuperclass();
        }
        return result;
    }
}
