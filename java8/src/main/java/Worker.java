import java.util.Objects;

public class Worker {
    public int id;
     private  int age;
     private  int  countBook;
     private int countFish;

    public Worker(int id, int age, int countBook, int countFish) {
        this.id = id;
        this.age = age;
        this.countBook = countBook;
        this.countFish = countFish;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public int getCountBook() {
        return countBook;
    }

    public int getCountFish() {
        return countFish;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCountBook(int countBook) {
        this.countBook = countBook;
    }

    public void setCountFish(int countFish) {
        this.countFish = countFish;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "id=" + id +
                ", age=" + age +
                ", countBook=" + countBook +
                ", countFish=" + countFish +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return id == worker.id &&
                age == worker.age &&
                countBook == worker.countBook &&
                countFish == worker.countFish;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, age, countBook, countFish);
    }
}
