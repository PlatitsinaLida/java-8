public class Student extends Human{
    public Student(String lastName, String firstName, String middleName, int age) {
        super(lastName, firstName, middleName, age);
    }

    public Student(Human copy) {
        super(copy);
    }
}
