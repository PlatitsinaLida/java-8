import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestReflectionDemo {
    @Test
    public void testCountHuman() {

        List<Object> objectList = new ArrayList<>();
        Human h = null;
        objectList.add(new Human("A", "Lhbkj", "atolevna", 1998));
        objectList.add(new Human("O", "Linlk", "levna", 1997));
        objectList.add(new Human("E", "nmnenko", "tolevna", 1995));
        objectList.add(new Worker(1, 18, 2, 5));
        objectList.add(new Object());
        objectList.add(h);
        objectList.add("dfg");
        objectList.add(new Student("Ch", "Lkj", "lna", 198));
        objectList.add(new Student("C", "Lj", "na", 19));
        Assert.assertEquals(ReflectionDemo.countHuman(objectList), 5);
    }
    @Test
    public void testListName() {
        List<String> result = new ArrayList<String>();
        Group group = new Group(1);
        result.add("getId");
        result.add("setId");
        result.add("setArrayInt");
        result.add("getArrayInt");
        result.add("getLength");
        result.add("getClass");
        result.add("hashCode");
        result.add("equals");
        result.add("toString");
        result.add("notify");
        result.add("notifyAll");
        result.add("wait");
        result.add("wait");
        result.add("wait");
        Assert.assertTrue(result.containsAll(ReflectionDemo.listOfPublicMethod(group)));
        Assert.assertTrue(ReflectionDemo.listOfPublicMethod(group).containsAll(result));
    }
    @Test
    public void testSuperClassName() {
        List<String> result = new ArrayList<String>();
        Student student = new Student("EN", "osdfgh", "a", 1998);
        result.add("Human");
        result.add("Object");
        Assert.assertTrue(result.containsAll(ReflectionDemo.listOfSuperClass(student)));
        Assert.assertTrue(ReflectionDemo.listOfSuperClass(student).containsAll(result));

    }


}
